/* #10.13 Я описываю все переменные в дополнительном файле в папке configuration */
/* #10.14 Вырежем из файла ^app/src/index.js^ PORT and HOST и вставим их сюда : */
module.exports.port = process.env.PORT;
module.exports.host = process.env.HOST;

/* #10.15 Определим переменную из файла ^api/src/helpers/db.js строка #10.6^ и мы её тоже получаем через переменную окружения env.MONGO_URL: */
/* #10.15 TD Мы нигде не указываем полный путь к нашей БД и мы можем его конфигурировать снаружи поскольку путь для продакшена и локали разные : */
module.exports.db = process.env.MONGO_URL;

/* #18.5 Укажем в переменных окружения какой будет Base URL, чтобы мы точно знали какой у нас API снаружи, а не внутри нашего сервиса : */
/* #18.5 Добавим в module.exports еще одну переменную authApiUrl, и мы берем ее из ^process.env.AUTH_API_URL^ : */
module.exports.authApiUrl = process.env.AUTH_API_URL;