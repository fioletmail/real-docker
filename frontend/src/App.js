import logo from './logo.svg';
/* #20.13 Импортируем axios */
import axios from "axios";
import './App.css';

function App() {
/* #20.8 Создадим метод ^makeApiRequest^ и выведем в console.log чтобы убедиться что все работает */
 const makeApiRequest = () => {
   console.log("makeApiRequest");
     /* #20.14 И делаем запрос на URL, получаем response и console.log его */
      axios("/api/testwithcurrentuser").then(response => {
        console.log("response", response);
      });
 };
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload. CHANGE from Docker We are in dev fuck off USA, Burn capitalizm
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      /* #20.7 Добавим кнопку которая будет делать запрос на наш api. При клике кнопки вызов метода ^makeApiRequest^ */
      <button onClick={makeApiRequest}>Make api request</button>
    </div>
  );
}

export default App;
