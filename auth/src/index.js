/* #5.7 Зареквайрим express из наших node модулей */
const express = require("express");

/* #19.7 Зареквайрим axios : */
const axios = require('axios');

/* #11.2 Зареквайрим Mongoose : */
/*const mongoose = require("mongoose"); #13.7 Mongoose мы уже более не используем */

/* #10.12 Здесь затребуем ^connectDb^ : */
const { connectDb } = require("./helpers/db");

/* #10.16 Здесь затребуем ^ port, host, db ^ из файла api/src/configuration/index.js : */
/* #19.6 Добавим apiUrl в require: */
const { port, host, db, apiUrl } = require("./configuration");

/* #5.8 Создадим наше приложени просто вызвав express() */
const app = express()

/* #9.4 Тогда здесь из процесса мы можем узнать нашу переменную PORT : */
/*console.log('PORT', process.env.PORT) #9.5 */

/*/!* #9.5 Сделаем наши контейнеры STATELESS и не будем хранить состояние нашего приложения внутри: *!/
/!* #9.5 Создадим переменную ^port^ : *!/
const port = process.env.PORT;

/!* #9.10.1 Создадим переменную ^host^ : *!/
const host = process.env.HOST; #10.14 */

/*/!* #11.1 Объявим новую схему ^postSchema^, как описано в документации Mongoose : *!/
const postSchema = new mongoose.Schema({
    name: String
});
/!* #11.3 Создадим модель ^Post^ в которой будет использована ^postSchema^: *!/
const Post = mongoose.model("Post", postSchema); #13.3 Удалим схему и модель, ведь мы знаем что наша БД работает */

/* #10.10 Теперь создадим эту функцию ^const startServer^ : */
const startServer = () => {
    /* #10.11 И внутрь функции переместим ^app.listen^ который мы написали ранее, и он вызовется только после того как приконектились к БД : */
    app.listen(port, () => {
        /*console.log(`Started api service on port ${port}`);*/
        /* #13.6 И здесь переименуем на auth */
        console.log(`Started auth service on port ${port}`);
        console.log(`Our host is ${host}`);
        /* #10.17 И вывыедем в console.log our db : */
        console.log(`Our database is ${db}`);

        /*        /!* #11.4 И при старте наше приложения будем создавть Инстансы назовем их ^silence^: *!/
                const silence = new Post({ name: "Silence" });
                /!* #11.5 И дальше console.log это имя : *!/
                console.log(silence.name); // 'Silence'
                /!* #11.7 Давайте выведем в console.log целый silence чтобы увидеть, что это объект : *!/
                console.log(silence);*/

/*        /!* #11.9 Сначала методом ^find^ получим все наши посты и если нет ошибки отобразим из в ^console.log('posts', posts);^ : *!/
        Post.find(function (err, posts) {
            if (err) return console.error(err);
            console.log('posts', posts);
        }); #11.11 */

/*        /!* #11.11 Сохраним новый Post в БД : *!/
        const silence = new Post({ name: "Silence" });
        silence.save(function(err, savedSilence) {
            if (err) return console.error(err);
            /!*console.log("savedSilence", savedSilence); #12.18 *!/
            /!* #12.18 Теперь сохраним это так и посмотрим будет ли перебилжен наш сервис : *!/
            console.log("savedSilence with jopasы", savedSilence);
        }); #13.4 Удалим и что мы делали для тестирования нашей БД */
    });
}

/* #5.9 Теперь опишем роуты, здесь аргументы request и response */
app.get("/test", (req, res) => {
    /* #5.10 И при вызове роута /test мы будем видеть в Браузере */
    /*res.send("Our api server is working correctly"); #13.5 */
    /* #13.5 Переименуем на auth */
    res.send("Our auth server is working correctly");
});

/* #19.8 Добавим метод ^testwithapidata^  */
app.get("/testwithapidata", (req, res) => {
    /* #19.9 И здесь запрашиваем apiUrl, который получаем из переменных окружения и сконкотинируем его с ^/testapidata^ */
   axios.get(apiUrl + "/testapidata").then(response => {
       res.json({
          /* #19.10 И прочитаем данные из response */
          testapidata: response.data.testwithapi
       });
   });
});

/* #5.11 Запустим наш app сервер, вызовем порт 3000 */
/* #7.1.2 И 3000 - это тот порт, который мы указали, когда запускаем наш Вэб-сервер */
/*app.listen(3000, () => { #9.6 */
/* #9.6 и дальше будем использовать переменную ^port^, как видите теперь приложении ничего не знает от 3000 порте, а порт указан снаружи самого приложения : */
/* #9.7 TD Хороший тон сохранять все такие переменные в отдельный файл Конфигурации */
/*app.listen(port, () => {
    /!* #5.12 И напишем такой callback когда наш сервис запустится : *!/
    /!*console.log("Started api service"); #9.8 *!/
    /!* #9.8 Укажем на каком порту стартует наш сервис ^${port}`^ *!/
    console.log(`Started api service on port ${port}`);
    /!* #9.10.2 Укажем на каком ХОСТЕ стартует наш сервис ^${host}^ *!/
    console.log(`Our host is ${host}`);
}); #10.11 */

/* #18.2 Добавим метод для выдачи текущего юзера на моках, просто вернем JSON с данными текущего юзера */
app.get("/api/currentUser", (req, res) => {
   res.json({
       id: "1234",
       email: "foo@gmail.com"
   });
});

/* #10.8 Теперь используем конект здесь, сначала вызовем его ^connectDb()^ : */
connectDb()
    /* #10.9.1 Скопируем из примера в документации, если ошибка то colsole.log : */
    .on("error", console.log)
    /* #10.9.2 Если disconnected, то вызываем ^connectDb^ заново : */
    .on("disconnected", connectDb)
    /* #10.9.3 И когда коннект успешно установлем ^"open"^, мы хотим стартануть наш сервер функцией ^startServer^  : */
    .once("open", startServer);