/* #10.13 Я описываю все переменные в дополнительном файле в папке configuration */
/* #10.14 Вырежем из файла ^app/src/index.js^ PORT and HOST и вставим их сюда : */
module.exports.port = process.env.PORT;
module.exports.host = process.env.HOST;

/* #10.15 Определим переменную из файла ^api/src/helpers/db.js строка #10.6^ и мы её тоже получаем через переменную окружения env.MONGO_URL: */
/* #10.15 TD Мы нигде не указываем полный путь к нашей БД и мы можем его конфигурировать снаружи поскольку путь для продакшена и локали разные : */
module.exports.db = process.env.MONGO_URL;

/* #19.5 Добавим в конфигурацию apiUrl */
module.exports.apiUrl = process.env.API_URL;